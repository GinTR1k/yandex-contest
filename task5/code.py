import sys

str1 = sys.stdin.readline().strip()
str2 = sys.stdin.readline().strip()

def anagram(str1, str2):
    if sorted(str1) == sorted(str2):
        return True

    return False

print(int(anagram(str1, str2)))
