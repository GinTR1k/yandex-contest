import sys

j = sys.stdin.readline().strip()
s = sys.stdin.readline().strip()

count = sum([list(s).count(x) for x in set(j)])

print(count)
