import sys

tree = []
tree_count = int(sys.stdin.readline().strip())

counts = [0]
for i in range(0, tree_count):
    tree.append(int(sys.stdin.readline().strip()))

    if len(tree) > 1:
        if tree[i] == 1 and tree[i - 1] == 0:
            counts.append(1)
        elif tree[i] == tree[i - 1] == 1:
            counts[-1] += 1
    elif tree[i] == 1:
        counts.append(1)

print(max(counts))
