import sys

prev_num = None
lines_count = int(sys.stdin.readline().strip())

for i in range(0, lines_count):
    number = int(sys.stdin.readline().strip())

    if prev_num is None:
        prev_num = number
        print(number)
    elif number > prev_num:
        prev_num = number
        print(number)
