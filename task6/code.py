from sys import stdin

result = []

for i in range(0, int(stdin.readline().strip())):
    result.extend(map(int, stdin.readline().split()[1:]))

result.sort()
[print(m, end=' ') for m in result]

print()
